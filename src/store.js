import { createGlobalState } from 'react-hooks-global-state';

const initialState = {
	screen: 'LANDING',
	screenColor: '',
	userAnswerMap: [],
	timesTableNumber: 0
};

export const { GlobalStateProvider, useGlobalState } = createGlobalState(initialState);
