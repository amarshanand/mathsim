import React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/styles';

import theme from './theme';
import './App.css';

import { useGlobalState } from './store';

import Landing from './components/Landing';
import Home from './components/Home';
import Add from './components/Add';
import Multiply from './components/Multiply';
import Tables from './components/Tables';
import Word from './components/Word';

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		height: '100%'
	}
}));

const App = (props) => {
	const classes = useStyles();
	const [ screen ] = useGlobalState('screen');
	return (
		<MuiThemeProvider theme={theme}>
			<div className={classes.root}>
				{screen === 'LANDING' && <Landing />}
				{screen === 'HOME' && <Home />}
				{screen === 'ADD' && <Add />}
				{screen === 'MULTIPLY' && <Multiply />}
				{screen === 'TABLES' && <Tables />}
				{screen === 'WORD' && <Word />}
			</div>
		</MuiThemeProvider>
	);
};

export default App;
