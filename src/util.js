import { GiphyFetch } from '@giphy/js-fetch-api';
import { Howl } from 'howler';

/* get random integer between two numbers */
const getRandomInt = (min = 0, max = 100) => {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

/* speak the provided text */
const speak = (text = '', done = () => {}) => {
	if (!('speechSynthesis' in window)) return done();
	const speech = window.speechSynthesis;
	// if a speech is in progress, cancel it first
	speech.speaking && speech.cancel();
	const utterance = new SpeechSynthesisUtterance(text);
	utterance.onend = done;
	speech.speak(utterance);
};

/* play some basic sounds - http://soundbible.com */
const playSound = (type) =>
	new Howl({
		src: [ `sound/${type}.mp3` ]
	}).play();

// test of no mobile
const isMobile = () =>
	(/iPad|iPhone|iPod/.test(navigator.platform) ||
		(navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1)) &&
	!window.MSStream;
//const isMobile = () => true;

const shuffleArray = (array) => {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[ array[i], array[j] ] = [ array[j], array[i] ];
	}
	return array;
};
// get a random gif
let randomGifs = [
	{
		height: '200',
		url:
			'https://media1.giphy.com/media/l3HBbltOYjoNq/giphy.gif?cid=d58d268030ee8b58ff5ba857e55cff885003e31274f4df96&rid=giphy.gif',
		width: '200'
	}
];
const gf = new GiphyFetch('nMVvNCo4v1LOakmWiEI3nh3mI8OW7d8b');
const prepareGifs = async (term) => {
	const { data: gifs } = await gf.search(term, {
		sort: 'newest',
		lang: 'es',
		limit: 30
	});
	shuffleArray(gifs);
	gifs.forEach(({ images }) => {
		randomGifs.push({
			term,
			url: images.downsized_large.url,
			height: images.downsized_large.height,
			width: images.downsized_large.width
		});
	});
};
[ 'shrek', 'toystory', 'stickman' ].map((term) => prepareGifs(term));
const getRandomGif = () => {
	const gif = randomGifs ? randomGifs[getRandomInt(0, randomGifs.length - 1)] : null;
	new Image().src = gif.url;
	return gif;
};

// get a random smiley (happy or sad) - from https://www.animoticons.com
const smileys = {
	happy: [
		'https://www.animoticons.com/files/emotions/positive-smiley-faces/10.gif',
		'https://www.animoticons.com/files/emotions/happy-smiley-faces/61.gif',
		'https://www.animoticons.com/files/emotions/happy-smiley-faces/62.gif',
		'https://www.animoticons.com/files/emotions/happy-smiley-faces/7.gif',
		'https://www.animoticons.com/files/emotions/happy-smiley-faces/60.gif',
		'https://www.animoticons.com/files/emotions/happy-smiley-faces/66.gif',
		'https://www.animoticons.com/files/emotions/happy-smiley-faces/59.gif',
		'https://www.animoticons.com/files/emotions/positive-smiley-faces/8.gif',
		'https://www.animoticons.com/files/emotions/positive-smiley-faces/7.gif'
	],
	sad: [
		'https://www.animoticons.com/files/emotions/sad-smiley-faces/21.gif',
		'https://www.animoticons.com/files/emotions/disappointed-smiley-faces/2.gif',
		'https://www.animoticons.com/files/emotions/negative-smiley-faces/4.gif',
		'https://www.animoticons.com/files/emotions/sad-smiley-faces/12.gif',
		'https://www.animoticons.com/files/emotions/sad-smiley-faces/27.gif',
		'https://www.animoticons.com/files/emotions/teasing-smiley-faces/14.gif'
	]
};
const getSmiley = (type = 'happy') => smileys[type][getRandomInt(0, smileys[type].length - 1)];

// ref: https://stackoverflow.com/questions/14766951/convert-digits-into-words-with-javascript
const numToWords = (n) => {
	const arr = (x) => Array.from(x);
	const num = (x) => Number(x) || 0;
	const isEmpty = (xs) => xs.length === 0;
	const take = (n) => (xs) => xs.slice(0, n);
	const drop = (n) => (xs) => xs.slice(n);
	const reverse = (xs) => xs.slice(0).reverse();
	const comp = (f) => (g) => (x) => f(g(x));
	const not = (x) => !x;
	const chunk = (n) => (xs) => (isEmpty(xs) ? [] : [ take(n)(xs), ...chunk(n)(drop(n)(xs)) ]);
	let a = [
		'',
		'one',
		'two',
		'three',
		'four',
		'five',
		'six',
		'seven',
		'eight',
		'nine',
		'ten',
		'eleven',
		'twelve',
		'thirteen',
		'fourteen',
		'fifteen',
		'sixteen',
		'seventeen',
		'eighteen',
		'nineteen'
	];
	let b = [ '', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety' ];
	let g = [
		'',
		'thousand',
		'million',
		'billion',
		'trillion',
		'quadrillion',
		'quintillion',
		'sextillion',
		'septillion',
		'octillion',
		'nonillion'
	];
	// this part is really nasty still
	// it might edit this again later to show how Monoids could fix this up
	let makeGroup = ([ ones, tens, huns ]) => {
		return [
			num(huns) === 0 ? '' : a[huns] + ' hundred ',
			num(ones) === 0 ? b[tens] : (b[tens] && b[tens] + ' ') || '',
			a[tens + ones] || a[ones]
		].join('');
	};
	// "thousands" constructor; no real good names for this, i guess
	let thousand = (group, i) => (group === '' ? group : `${group} ${g[i]}`);
	// execute !
	if (typeof n === 'number') return numToWords(String(n));
	if (n === '0') return 'zero';
	return comp(chunk(3))(reverse)(arr(n)).map(makeGroup).map(thousand).filter(comp(not)(isEmpty)).reverse().join(' ');
};

export { getRandomInt, getRandomGif, isMobile, speak, playSound, getSmiley, numToWords };
