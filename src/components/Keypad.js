import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';

import { useGlobalState } from '../store';
import { playSound } from '../util';

const useStyles = makeStyles((theme) => ({
	root: {
		color: '#ffffff',
		borderRadius: '10px',
		boxShadow:
			'0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)'
	},
	td: {
		width: '3.5rem',
		height: '3.5rem',
		border: '1px solid rgba(0, 0, 0, 0.1)',
		textAlign: 'center',
		borderRadius: '10px'
	}
}));

function Keypad({ init = '', onChange, invert = true, signs = false }) {
	const classes = useStyles();
	const [ screenColor ] = useGlobalState('screenColor');

	const onKeyPressed = (char) => {
		playSound('click');
		let _value = init;
		if (_value === '' && (char === '⌫' || char === '⌧')) return;
		if (_value && char === '⌫') _value = invert ? _value.substring(1) : _value.slice(0, -1);
		else if (char === '⌧') _value = '';
		else if ([ '+', '-', 'x', '÷', '' ].includes(char)) _value = char;
		else _value = invert ? char + _value : _value + char;
		onChange(_value);
	};

	const Cell = ({ char }) => (
		<td className={classes.td} style={{ backgroundColor: screenColor }} onClick={() => onKeyPressed(char)}>
			<Typography variant="h4">{char}</Typography>
		</td>
	);

	return (
		<table className={classes.root}>
			<tbody>
				<tr>{signs ? <Cell char={'+'} /> : [ 1, 2, 3 ].map((c) => <Cell key={c} char={c} />)}</tr>
				<tr>{signs ? <Cell char={'-'} /> : [ 4, 5, 6 ].map((c) => <Cell key={c} char={c} />)}</tr>
				<tr>{signs ? <Cell char={'x'} /> : [ 7, 8, 9 ].map((c) => <Cell key={c} char={c} />)}</tr>
				<tr>{signs ? <Cell char={'÷'} /> : [ '⌫', 0, '⌧' ].map((c) => <Cell key={c} char={c} />)}</tr>
			</tbody>
		</table>
	);
}

export default Keypad;
