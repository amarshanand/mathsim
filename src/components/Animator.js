import React from 'react';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
	root: {
		position: 'absolute',
		zIndex: 1111111111
	},
	paper: {
		position: 'relative'
	},
	image: {
		position: 'absolute',
		backgroundColor: 'rgb(255,255,255,0.6)',
		boxShadow: '0px 0px 100px #fff'
	}
}));

export default function Animator({ urlObj, timeout = 8000 }) {
	const classes = useStyles();

	if (!urlObj || !urlObj.url)
		urlObj = {
			height: '270',
			term: 'angry birds',
			url:
				'https://media3.giphy.com/media/26n7b4nROfNWWNmcE/giphy.gif?cid=d58d26803350c9596012008af892d2d014052e83ddd00858&rid=giphy.gif',
			width: '480'
		};

	const { url, height, width } = urlObj;

	return (
		<div
			className={classes.root}
			style={{
				right: -1 * width,
				bottom: 0.5 * height
			}}
		>
			<Slide direction="right" in={true} mountOnEnter unmountOnExit timeout={timeout}>
				<Paper
					elevation={4}
					className={classes.paper}
					style={{
						margin: -0.5 * width
					}}
				>
					<img alt="" className={classes.image} src={url} />
				</Paper>
			</Slide>
		</div>
	);
}
