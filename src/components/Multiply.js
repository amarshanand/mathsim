import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';

import Keypad from './Keypad';
import Animator from './Animator';
import TopNav from './TopNav';
import BottomBar from './BottomBar';

import { getRandomInt, getRandomGif, speak, playSound, getSmiley } from '../util';
import { useGlobalState } from '../store';

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		height: '100%',
		backgroundColor: '#eeeeee',
		color: '#333333'
	},
	question: {
		display: 'inline-block',
		margin: '0 auto'
	},
	userAnswer: {
		height: '3.5rem',
		minWidth: '8rem',
		borderBottom: '3px solid',
		marginLeft: '1rem',
		marginTop: '0.5rem',
		letterSpacing: '0.5rem'
	},
	keypad: {
		position: 'absolute',
		bottom: '3rem',
		right: '0',
		width: '12rem'
	},
	answer: {
		position: 'fixed',
		bottom: '4rem',
		left: 0,
		color: 'white',
		padding: '1rem'
	},
	originalQuestion: {
		marginTop: '3rem',
		padding: '2rem 0 0 2rem'
	},
	repeatedAddition: {
		padding: '2rem 0 0 2rem'
	},
	container: {
		width: 'calc(100% - 2rem)',
		display: 'flex',
		flexWrap: 'wrap',
		padding: '2rem 0 0 2rem'
	},
	group: {
		maxWidth: '10rem',
		border: '3px solid',
		borderRadius: '6px',
		margin: '0 2rem 2rem 0',
		padding: '1rem 0 0 1rem'
	},
	dots: {
		display: 'inline-block',
		borderRadius: '50%',
		width: '1rem',
		height: '1rem',
		margin: '0 1rem 1rem 0'
	},
	controls: {
		margin: '3rem 2rem'
	},
	button: { width: '4rem', color: 'rgba(255,255,255,0.8)' },
	totalContainer: {
		width: 'calc(100% - 26rem)',
		display: 'inline-flex',
		flexWrap: 'wrap',
		padding: '1rem 0 0 1rem',
		borderRadius: '6px',
		marginLeft: '4rem'
	}
}));

const Question = ({ userAnswer }) => {
	const classes = useStyles();
	return (
		<table className={classes.question}>
			<thead>
				<tr>
					<td>
						<Typography variant="h3" style={{ textAlign: 'center' }}>
							{`${first} ${op} ${second} = `}
						</Typography>
					</td>
					<td>
						<Typography variant="h3" style={{ textAlign: 'center' }}>
							<div className={classes.userAnswer}>{userAnswer}</div>
						</Typography>
					</td>
				</tr>
			</thead>
		</table>
	);
};

const RepeatedAddition = () => {
	const array = [];
	for (let i = 0; i < second; i++) array.push(first);
	return <Typography variant="h3">{array.join(' + ')}</Typography>;
};

let first, second, op, answer;

const reset = (init) => {
	first = init ? init.first : getRandomInt(2, 20);
	second = init ? init.second : getRandomInt(2, 10);
	op = init ? init.op : second % 2 === 0 ? 'x' : '÷';
	if (op === 'x') answer = `${first * second}`;
	else {
		answer = `${first}`;
		first *= second;
	}
	answer = init ? init.answer : answer;
	!init && speak(`What is ${first} ${op === 'x' ? 'times' : 'divided by'} ${second}?`);
};

const Multiply = ({ init, onCorrect }) => {
	const classes = useStyles();
	const [ userAnswerMap, setUserAnswerMap ] = useGlobalState('userAnswerMap');
	const [ screenColor ] = useGlobalState('screenColor');
	const [ urlObj, setUrlObj ] = useState();
	const [ showBack, setShowBack ] = useState(true);
	const [ showAnimation, setShowAnimation ] = useState(false);
	const [ userAnswer, setUserAnswer ] = useState(null);
	const [ localUserAnswerMap, setLocalUserAnswerMap ] = useState(userAnswerMap);
	const [ hintLevel, setHintLevel ] = useState(0);
	const [ userDotsPerGroup, setUserDotsPerGroup ] = useState(0);
	const [ userNoOfGroups, setUserNoOfGroups ] = useState(0);

	const Pictorial = () => {
		const classes = useStyles();
		return (
			<div>
				<div className={classes.controls}>
					{/* no of groups */}
					<ButtonGroup>
						<Button
							className={classes.button}
							style={{ backgroundColor: screenColor }}
							disabled={userNoOfGroups <= 0}
							onClick={() => {
								playSound('drop1');
								setUserNoOfGroups(userNoOfGroups - 1);
							}}
						>
							<Typography variant="h4">-</Typography>
						</Button>
						<Button
							className={classes.button}
							style={{ backgroundColor: screenColor }}
							disabled={userNoOfGroups >= 10}
							onClick={() => {
								playSound('drop1');
								setUserNoOfGroups(userNoOfGroups + 1);
							}}
						>
							<Typography variant="h4">+</Typography>
						</Button>
					</ButtonGroup>
					{/* no of dots per group */}
					<ButtonGroup style={{ marginLeft: '4rem', opacity: userNoOfGroups <= 0 ? 0.5 : 1 }}>
						<Button
							className={classes.button}
							style={{ borderColor: screenColor }}
							disabled={userNoOfGroups <= 0 || userDotsPerGroup <= 0}
							onClick={() => {
								playSound('drop2');
								setUserDotsPerGroup(userDotsPerGroup - 1);
							}}
						>
							<Typography variant="h4" style={{ color: screenColor }}>
								-
							</Typography>
						</Button>
						<Button
							className={classes.button}
							style={{ borderColor: screenColor }}
							disabled={userNoOfGroups <= 0 || userDotsPerGroup >= 10}
							onClick={() => {
								playSound('drop2');
								setUserDotsPerGroup(userDotsPerGroup + 1);
							}}
						>
							<Typography variant="h4" style={{ color: screenColor }}>
								+
							</Typography>
						</Button>
					</ButtonGroup>
					{/* total dots (for division) */}
					{op !== 'x' && (
						<div
							className={classes.totalContainer}
							style={{ border: `2px solid ${screenColor}`, background: `${screenColor}22` }}
						>
							{[ ...Array(parseInt(first)).keys() ].map((i) => (
								<div
									key={i}
									className={classes.dots}
									style={{
										backgroundColor:
											i < first - userNoOfGroups * userDotsPerGroup ? screenColor : 'transparent'
									}}
								/>
							))}
						</div>
					)}
				</div>
				{/* dots for the user */}
				<div className={classes.container}>
					{[ ...Array(userNoOfGroups).keys() ].map((i) => (
						<div key={i} className={classes.group} style={{ borderColor: screenColor }}>
							{[ ...Array(userDotsPerGroup).keys() ].map((j) => (
								<div key={j} className={classes.dots} style={{ backgroundColor: screenColor }} />
							))}
						</div>
					))}
				</div>
			</div>
		);
	};

	const showNext = () => {
		reset(init);
		setUrlObj(getRandomGif());
		setUserAnswer(null);
		setShowAnimation(false);
		setShowBack(true);
		setHintLevel(0);
		setUserDotsPerGroup(0);
		setUserNoOfGroups(0);
		setTimeout(() => setShowBack(false), 5 * 1000);
	};
	!answer && showNext();

	// eslint-disable-next-line react-hooks/exhaustive-deps
	useEffect(
		() => {
			if (init && userAnswer && userAnswer === init.answer) return onCorrect(userAnswer);
			// the user asked to show the answer
			if (!init && userAnswer && userAnswer === answer) {
				// the user entered the correct answer
				playSound('fail');
				setShowAnimation(true);
				speak();
				setTimeout(() => {
					showNext();
					playSound('ding');
					setLocalUserAnswerMap([ ...localUserAnswerMap, getSmiley('happy') ]);
				}, 6000);
			}
		},
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[ init, onCorrect, userAnswer, localUserAnswerMap ]
	);

	useEffect(() => () => setUserAnswerMap(localUserAnswerMap));
	const isSHowHint = first !== 0 && second !== 0;
	const raiseHintLevel = () => {
		let _hintLevel = hintLevel + (op === 'x' ? 1 : 2);
		if (_hintLevel > 2) return;
		setHintLevel(_hintLevel);
		_hintLevel === 1 && op === 'x' && speak(`Add ${first}. ${second} times.`);
		//_hintLevel === 2 && op === 'x' && speak(`Draw ${second} groups, and draw ${first} dots in each group.`);
		_hintLevel === 2 &&
			op === '÷' &&
			speak(`Draw ${second} groups, and distribute ${first} dots amongst ${second} groups.`);
	};
	first = init ? init.first : first;
	second = init ? init.second : second;
	op = init ? init.op : op;
	answer = init ? init.answer : answer;
	return (
		<div className={classes.root} onClick={raiseHintLevel}>
			{!init && <TopNav heading="Multiply and Divide" onShowNext={showNext} showBack={showBack} />}
			<div className={classes.originalQuestion}>
				<Question userAnswer={userAnswer} />
			</div>
			{isSHowHint &&
			userAnswer !== answer &&
			op === 'x' &&
			hintLevel > 0 && (
				<div className={classes.repeatedAddition}>
					<RepeatedAddition />
				</div>
			)}
			{userAnswer !== answer &&
			isSHowHint &&
			hintLevel > 1 &&
			op !== 'x' /* op !== 'x' removes pictorial hints from multiplacation. Jai knows tables now */ && (
				<div className={classes.pictorial}>
					<Pictorial />
				</div>
			)}
			{!showAnimation && (
				<div className={classes.keypad}>
					<Keypad
						init={`${userAnswer || ''}`}
						invert={false}
						onChange={(value) => value.length <= 3 && setUserAnswer(value)}
					/>
				</div>
			)}
			{!init && showAnimation && <Animator urlObj={urlObj} />}
			{!init && <div className={classes.answer}>{answer}</div>}
			{!init && <BottomBar userAnswerMap={localUserAnswerMap} />}
		</div>
	);
};

export default Multiply;
