import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';

import Keypad from './Keypad';
import Dotter from './Dotter';
import Animator from './Animator';
import TopNav from './TopNav';
import BottomBar from './BottomBar';

import { getRandomInt, getRandomGif, speak, playSound, getSmiley } from '../util';
import { useGlobalState } from '../store';

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		height: '100%',
		backgroundColor: '#eeeeee',
		color: '#333333'
	},
	table: {
		width: '100%',
		height: 'calc(100% + 3rem)'
	},
	question: {
		borderCollapse: 'collapse',
		margin: '0 auto'
	},
	td: {
		width: '4rem',
		height: '4rem',
		border: '1px solid rgba(0, 0, 0, 0.1)'
	},
	firstRow: {
		border: '1px dotted rgba(0, 0, 0, 0.1)',
		borderLeft: 'none',
		display: 'none'
	},
	secondRow: {
		borderBottom: '2px solid black'
	},
	keypad: {
		position: 'absolute',
		bottom: '3rem',
		right: '0'
	},
	answer: {
		position: 'fixed',
		bottom: '4rem',
		left: 0,
		color: 'white',
		padding: '1rem'
	}
}));

const Question = ({ userAnswer, init }) => {
	const classes = useStyles();
	const f = `${init ? init.first : first}`.padStart(3, ' ').match(/.{1,1}/g);
	const s = `${init ? init.second : second}`.padStart(3, ' ').match(/.{1,1}/g);
	const ua = !userAnswer ? `    ` : `${userAnswer}`.padStart(4, ' ').match(/.{1,1}/g);
	const Cell = ({ text, pos }) => {
		let bg = 'none';
		if (userAnswer === null && pos === 3) bg = 'yellow';
		if (('' + userAnswer).length === 3 - pos) bg = 'yellow';
		if (userAnswer && userAnswer === answer) bg = 'none';
		return (
			<td className={classes.td} style={{ backgroundColor: bg }}>
				<Typography variant="h3" style={{ textAlign: 'center' }}>
					{text}
				</Typography>
			</td>
		);
	};
	return (
		<table className={classes.question}>
			<tbody>
				<tr>
					<Cell />
					<Cell text={f[0]} />
					<Cell text={f[1]} />
					<Cell text={f[2]} />
				</tr>
				<tr className={classes.secondRow}>
					<Cell text={init ? init.op : op} />
					<Cell text={s[0]} />
					<Cell text={s[1]} />
					<Cell text={s[2]} />
				</tr>
				<tr className={classes.secondRow}>
					<Cell text={ua[0]} pos={0} />
					<Cell text={ua[1]} pos={1} />
					<Cell text={ua[2]} pos={2} />
					<Cell text={ua[3]} pos={3} />
				</tr>
			</tbody>
		</table>
	);
};

let first, second, op, answer;

const Add = ({ init, onCorrect }) => {
	const classes = useStyles();
	const [ showBack, setShowBack ] = useState(true);
	const [ showAnimation, setShowAnimation ] = useState(false);
	const [ userAnswer, setUserAnswer ] = useState(null);
	const [ urlObj, setUrlObj ] = useState(null);
	const [ showDots, setShowDots ] = useState(true);
	const [ userAnswerMap, setUserAnswerMap ] = useGlobalState('userAnswerMap');
	const [ localUserAnswerMap, setLocalUserAnswerMap ] = useState(userAnswerMap);

	const reset = () => {
		first = getRandomInt(0, 999);
		second = getRandomInt(0, first);
		op = second % 2 === 0 ? '+' : '-';
		answer = `${op === '+' ? first + second : first - second}`;
		!init && speak(`What is ${first} ${op === '+' ? 'plus' : 'minus'} ${second}?`);
	};

	// eslint-disable-next-line react-hooks/exhaustive-deps
	const showNext = () => {
		reset();
		setUserAnswer(null);
		setShowAnimation(false);
		setShowBack(true);
		setUrlObj(getRandomGif());
		setTimeout(() => setShowBack(false), 5 * 1000);
	};
	if (!answer) showNext();

	useEffect(
		() => {
			if (init && userAnswer && userAnswer === init.answer) return onCorrect(userAnswer);
			if (userAnswer && userAnswer === answer) {
				// the user entered the correct answer
				playSound('fail');
				setShowAnimation(true);
				setTimeout(() => {
					showNext();
					playSound('ding');
					setLocalUserAnswerMap([ ...localUserAnswerMap, getSmiley('happy') ]);
				}, 6000);
			}
		},
		[ init, onCorrect, showNext, userAnswer, localUserAnswerMap ]
	);

	useEffect(() => () => setUserAnswerMap(localUserAnswerMap));
	return (
		<div className={classes.root}>
			{!init && <TopNav heading="Add and Subtract" onShowNext={showNext} showBack={showBack} />}
			{!showAnimation && showDots && <Dotter />}
			<table className={classes.table}>
				<tbody>
					<tr>
						<td>
							<Question userAnswer={userAnswer} init={init} />
						</td>
					</tr>
				</tbody>
			</table>
			{!showAnimation && (
				<div className={classes.keypad}>
					<Keypad
						init={`${userAnswer || ''}`}
						onChange={(value) => {
							setUserAnswer(value);
							value.length <= 4 && setUserAnswer(value);
							setShowDots(false);
							setTimeout(() => setShowDots(true), 0);
						}}
					/>
				</div>
			)}
			{!init && showAnimation && <Animator urlObj={urlObj} />}
			{!init && <div className={classes.answer}>{answer}</div>}
			{!init && <BottomBar userAnswerMap={localUserAnswerMap} />}
		</div>
	);
};

export default Add;
