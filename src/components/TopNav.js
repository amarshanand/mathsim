import React from 'react';
import { makeStyles } from '@material-ui/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeft from '@material-ui/icons/ChevronLeft';

import { useGlobalState } from '../store';

import { playSound } from '../util';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1
	},
	title: {
		flexGrow: 1,
		textAlign: 'center'
	},
	answer: {
		width: '1.5rem'
	}
}));

function TopNav({ heading, showBack = true }) {
	const classes = useStyles();
	const [ screenColor ] = useGlobalState('screenColor');
	const [ , setScreen ] = useGlobalState('screen');
	return (
		<AppBar position="fixed" style={{ backgroundColor: screenColor }}>
			<Toolbar>
				<IconButton
					disabled={!showBack}
					edge="start"
					color="inherit"
					onClick={() => {
						playSound('click');
						setScreen('HOME');
					}}
				>
					<ChevronLeft />
				</IconButton>
				<Typography variant="h6" color="inherit" className={classes.title}>
					{heading}
				</Typography>
			</Toolbar>
		</AppBar>
	);
}

export default TopNav;
