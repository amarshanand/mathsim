import React from 'react';
import { makeStyles } from '@material-ui/styles';

import { useGlobalState } from '../store';

import { speak } from '../util';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        height: '100%',
        backgroundColor: '#00BCD4',
        textAlign: 'center',
        color: 'white'
    }
}));

function Landing() {
    const classes = useStyles();
    const [ , setScreen ] = useGlobalState('screen');
    return (
        <table
            className={classes.root}
            onClick={() => {
                speak('Dingo');
                speak(`Hello Jay. What would you like to do?`);
                setScreen('HOME');
                document.getElementById('root').requestFullscreen &&
                    document.getElementById('root').requestFullscreen();
            }}
        >
            <tbody>
                <tr>
                    <td>
                        <Typography variant="h3" color="inherit">
                            Tap here to get started
                        </Typography>
                    </td>
                </tr>
            </tbody>
        </table>
    );
}

export default Landing;
