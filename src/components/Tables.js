import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';

import Keypad from './Keypad';
import Animator from './Animator';
import TopNav from './TopNav';
import BottomBar from './BottomBar';

import { getRandomGif, speak, playSound, getSmiley } from '../util';
import { useGlobalState } from '../store';

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		height: '100%',
		backgroundColor: '#eeeeee',
		color: '#333333'
	},
	keypad: {
		position: 'absolute',
		bottom: '3rem',
		right: '0',
		width: '12rem'
	},
	dot: {
		display: 'inline-block',
		width: '6px',
		height: '3em',
		background: '#e91e63'
	},
	wide: {
		width: '3px',
		marginLeft: '8px'
	},
	container: {
		maxHeight: `calc(100% - 4rem)`,
		overflowY: 'scroll'
	},
	table: {
		marginTop: '4rem',
		width: 'calc(100% - 12rem)'
	},
	questionTD: {
		width: '10rem',
		maxWidth: '10rem'
	},
	answerTD: {
		width: '4rem',
		maxWidth: '4rem'
	},
	userAnswer: {
		height: '2.5rem',
		minWidth: '4rem',
		borderBottom: '2px solid',
		marginLeft: '1rem',
		marginTop: '0.5rem',
		textAlign: 'right'
	},
	hintTD: {
		paddingLeft: '2rem'
	},
	trick: {
		position: 'fixed',
		top: '5rem',
		right: '1rem',
		padding: '0.5rem 1rem',
		background: 'lightyellow',
		borderRadius: '8px',
		border: '2px solid yellow',
		color: 'rgba(0,0,0,0.5)',
		fontSize: '3rem'
	}
}));

let second = 1,
	answer;

const firstGif = getRandomGif();
const Tables = () => {
	const classes = useStyles();
	const [ userAnswerMap, setUserAnswerMap ] = useGlobalState('userAnswerMap');
	const [ , setScreen ] = useGlobalState('screen');
	const [ screenColor ] = useGlobalState('screenColor');
	const [ first ] = useGlobalState('timesTableNumber');
	const [ urlObj, setUrlObj ] = useState(firstGif);
	const [ showBack, setShowBack ] = useState(true);
	const [ showAnimation, setShowAnimation ] = useState(false);
	const [ userAnswer, setUserAnswer ] = useState(null);
	const [ localUserAnswerMap, setLocalUserAnswerMap ] = useState(userAnswerMap);
	const [ hintLevel, setHintLevel ] = useState(0);

	!answer && speak(`What is ${first} times ${second}?`);
	answer = `${first * second}`;

	useEffect(
		() => {
			// the user entered the correct answer
			if (userAnswer && userAnswer === answer) {
				playSound('bell');
				setShowAnimation(true);
				setTimeout(() => {
					second++;
					setShowAnimation(false);
					setLocalUserAnswerMap([ ...localUserAnswerMap, getSmiley('happy') ]);
					if (second >= 11) {
						return speak(`Well done Jai. You have finished times table for ${first}`, () => {
							second = 1;
							answer = null;
							setScreen('HOME');
						});
					}
					setUrlObj(getRandomGif());
					setUserAnswer(null);
					setHintLevel(0);
					setTimeout(() => speak(`What is ${first} times ${second}?`), 500);
					setTimeout(() => setShowBack(false), 5 * 1000);
				}, 4000);
			}
		},
		[ first, setScreen, userAnswer, localUserAnswerMap ]
	);

	const Dots = ({ size }) => (
		<div>
			{[ ...Array(size).keys() ].map((i) =>
				[ ...Array(first).keys() ].map((j) => (
					<div
						key={i + j}
						className={`${classes.dot} ${size === second && i === size - 1 ? classes.wide : ''}`}
					/>
				))
			)}
		</div>
	);

	useEffect(() => () => setUserAnswerMap(localUserAnswerMap));
	const element = document.getElementById('tablesContainer');
	if (element) element.scrollTop = element.scrollHeight;
	return (
		<div
			className={classes.root}
			onClick={() => {
				setHintLevel(hintLevel + 1);
			}}
		>
			<TopNav heading="Multiplication Table" showBack={showBack} />
			<div id="tablesContainer" className={classes.container}>
				{(first === 8 || first === 9) && (
					<div className={classes.trick}>
						{first} = 10 -{' '}
						<span style={{ color: screenColor, fontWeight: 'bold' }}>{first === 8 ? 2 : 1}</span>
					</div>
				)}
				<table className={classes.table}>
					<tbody>
						{[ ...Array(Math.min(second + 1, 11)).keys() ].map(
							(i) =>
								i !== 0 && (
									<tr key={i}>
										<td className={classes.questionTD}>
											<Typography variant="h4" style={{ textAlign: 'right' }}>
												{`${first} x ${i} = `}
											</Typography>
										</td>
										<td className={classes.answerTD}>
											<Typography variant="h4">
												<div
													className={classes.userAnswer}
													style={{ background: i === second ? 'yellow' : 'transparent' }}
												>
													{i === second ? userAnswer : first * i}
												</div>
											</Typography>
										</td>
										<td className={classes.hintTD}>
											{(i !== second || hintLevel > 0) && first === 7 && <Dots size={i} />}
										</td>
										<td />
									</tr>
								)
						)}
					</tbody>
				</table>
			</div>
			{!showAnimation &&
			second <= 10 && (
				<div className={classes.keypad}>
					<Keypad
						init={`${userAnswer || ''}`}
						invert={false}
						onChange={(value) => value.length <= 3 && setUserAnswer(value)}
					/>
				</div>
			)}
			{showAnimation && <Animator urlObj={urlObj} timeout={6000} />}
			<BottomBar userAnswerMap={localUserAnswerMap} />
		</div>
	);
};

export default Tables;
