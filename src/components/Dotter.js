import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';

import { useGlobalState } from '../store';
import { playSound } from '../util';

const useStyles = makeStyles((theme) => ({
	root: {
		position: 'fixed',
		top: '3.5rem',
		left: '0',
		width: '100%',
		borderCollapse: 'collapse',
		zIndex: 1111
	},
	td: {
		textAlign: 'center',
		height: '4rem'
	},
	dot: {
		display: 'inline-block',
		width: '1rem',
		height: '1rem',
		borderRadius: '50%',
		border: '2px solid'
	}
}));

function Dotter() {
	const classes = useStyles();
	const [ active, setActive ] = useState([]);
	const [ screenColor ] = useGlobalState('screenColor');
	return (
		<table className={classes.root}>
			<tbody>
				<tr>
					{[ ...Array(15).keys() ].map((i) => (
						<td
							key={i}
							className={classes.td}
							onClick={() => {
								playSound('drop2');
								if (active.includes(i)) setActive(active.filter((j) => j !== i));
								else setActive([ ...active, i ]);
							}}
						>
							<div
								className={classes.dot}
								style={{
									borderColor: screenColor,
									backgroundColor: active.includes(i) ? screenColor : 'transparent'
								}}
							/>
						</td>
					))}
				</tr>
			</tbody>
		</table>
	);
}

export default Dotter;
