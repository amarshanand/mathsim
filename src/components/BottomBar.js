import React from 'react';
import { makeStyles } from '@material-ui/styles';

import { useGlobalState } from '../store';

const useStyles = makeStyles((theme) => ({
	root: {
		position: 'fixed',
		width: '100%',
		maxWidth: '100%',
		overflowX: 'scroll',
		height: '3rem',
		bottom: 0,
		padding: '0 1rem'
	},
	icon: {
		display: 'inline-block',
		width: '2.5rem',
		height: '2.5rem',
		backgroundSize: 'cover',
		margin: '0.25rem 1rem 0 0'
	}
}));

function BottomBar({ userAnswerMap }) {
	const classes = useStyles();
	const [ screenColor ] = useGlobalState('screenColor');
	const element = document.getElementById('bottomBar');
	if (element) element.scrollTop = element.scrollHeight;
	return (
		<div id="bottomBar" className={classes.root} style={{ backgroundColor: screenColor }}>
			{userAnswerMap.map((res, i) => (
				<div key={i} className={classes.icon} style={{ backgroundImage: `url(${res})` }} />
			))}
		</div>
	);
}

export default BottomBar;
