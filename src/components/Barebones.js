/**
 * A starting point for various types of questions
 */

import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';

import Keypad from './Keypad';
import Animator from './Animator';
import TopNav from './TopNav';
import BottomBar from './BottomBar';

import { getRandomInt, getRandomGif, speak, playSound, getSmiley } from '../util';
import { useGlobalState } from '../store';

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		height: '100%',
		backgroundColor: '#eeeeee',
		color: '#333333'
	},
	question: {
		padding: '5rem 2rem 1rem 2rem',
		lineHeight: '3rem'
	},
	userAnswer: {
		margin: '2rem',
		textAlign: 'center',
		fontSize: '3rem',
		fontWeight: 'bold'
	},
	keypad: {
		position: 'absolute',
		bottom: '3rem',
		right: '0',
		width: '12rem'
	},
	answer: {
		position: 'fixed',
		bottom: '4rem',
		left: 0,
		color: 'white',
		padding: '1rem'
	}
}));

const Question = ({ userAnswer }) => {
	const classes = useStyles();
	return (
		<Typography variant="h4" className={classes.question}>
			Question
		</Typography>
	);
};

let first, second, op, answer;

const reset = () => {
	first = getRandomInt(1, 99);
	second = getRandomInt(1, 99);
	op = second < first ? '+' : '-';
	answer = op === '+' ? first + second : first - second;
	speak('speak something');
};

const Word = () => {
	const classes = useStyles();
	const [ userAnswerMap, setUserAnswerMap ] = useGlobalState('userAnswerMap');
	const [ urlObj, setUrlObj ] = useState();
	const [ showBack, setShowBack ] = useState(true);
	const [ showAnimation, setShowAnimation ] = useState(false);
	const [ userAnswer, setUserAnswer ] = useState(null);
	const [ localUserAnswerMap, setLocalUserAnswerMap ] = useState(userAnswerMap);
	const [ hintLevel, setHintLevel ] = useState(0);

	const showNext = () => {
		reset();
		setUrlObj(getRandomGif());
		setUserAnswer(null);
		setShowAnimation(false);
		setShowBack(true);
		setHintLevel(0);
		setTimeout(() => setShowBack(false), 5 * 1000);
	};
	!answer && showNext();

	useEffect(
		() => {
			// the user asked to show the answer
			if (userAnswer && userAnswer === answer) {
				// the user entered the correct answer
				playSound('fail');
				setShowAnimation(true);
				speak();
				setTimeout(() => {
					showNext();
					playSound('ding');
					setLocalUserAnswerMap([ ...localUserAnswerMap, getSmiley('happy') ]);
				}, 6000);
			}
		},
		[ userAnswer, localUserAnswerMap ]
	);

	useEffect(() => () => setUserAnswerMap(localUserAnswerMap));

	return (
		<div className={classes.root}>
			<TopNav heading="Word Problems" onShowNext={showNext} showBack={showBack} />
			<Question userAnswer={{ first: 35, second: 23, op: '+', answer: 12 }} />
			{!showAnimation && (
				<div className={classes.keypad}>
					<Keypad invert={false} onChange={(value) => value.length <= 3 && setUserAnswer(value)} />
				</div>
			)}
			{showAnimation && <Animator urlObj={urlObj} />}
			<div className={classes.answer}>{answer}</div>
			<BottomBar userAnswerMap={localUserAnswerMap} />
		</div>
	);
};

export default Word;
