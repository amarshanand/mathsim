import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';

import Keypad from './Keypad';
import Animator from './Animator';
import TopNav from './TopNav';
import BottomBar from './BottomBar';

import { getRandomInt, getRandomGif, speak, playSound, getSmiley, numToWords } from '../util';
import { useGlobalState } from '../store';
import Add from './Add';
import Multiply from './Multiply';

const { names, objects, questions } = require('../word.json');

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		height: '100%',
		backgroundColor: '#eeeeee',
		color: '#333333'
	},
	question: {
		padding: '6.5rem 2rem 1rem 2rem',
		lineHeight: '3rem'
	},
	first: { color: '#cc0000' },
	second: { color: '#0000cc' },
	userAnswer: {
		margin: '2rem auto',
		textAlign: 'center',
		fontSize: '3rem',
		fontWeight: 'bold'
	},
	userAnswerPart: {
		display: 'inline-block',
		width: '6rem',
		height: '3rem',
		margin: '0.5rem',
		borderBottom: '3px solid black',
		color: 'blue'
	},
	userAnswerOp: {
		display: 'inline-block',
		width: '3rem',
		height: '3rem',
		margin: '0.5rem 1rem'
	},
	keypad: {
		position: 'absolute',
		bottom: '3rem',
		right: '0'
	},
	answer: {
		position: 'fixed',
		bottom: '4rem',
		left: 0,
		color: 'white',
		padding: '1rem'
	}
}));

let name1, name2, object, first, second, question, op, answer, init;

const reset = () => {
	init = '';
	question = questions[getRandomInt(0, questions.length - 1)];
	name1 = names[getRandomInt(0, names.length - 1)];
	name2 = names[getRandomInt(0, names.length - 1)];
	object = objects[getRandomInt(0, objects.length - 1)];
	if ([ 'addition', 'subtraction' ].includes(question.type)) {
		op = question.type === 'addition' ? '+' : '-';
		first = getRandomInt(1, 99);
		second = getRandomInt(1, op === '+' ? 99 : first);
		answer = op === '+' ? first + second : first - second;
	} else {
		op = question.type === 'multiplication' ? 'x' : '÷';
		first = getRandomInt(2, 10);
		second = getRandomInt(2, 10);
		if (op === 'x') answer = `${first * second}`;
		else {
			answer = `${first}`;
			first *= second;
		}
	}
	first = `${first}`;
	second = `${second}`;
	answer = `${answer}`;
	setTimeout(() => speak(document.getElementById('wordQuestion').innerText), 1000);
	question._text = question.text
		.replace(/_first/g, `<span class='first'>${numToWords(first)}</span>`)
		.replace(/_second/g, `<span class='second'>${numToWords(second)}</span>`)
		.replace(/_name1/g, name1)
		.replace(/_name2/g, name2)
		.replace(/_object/g, object);
};

const Word = () => {
	const classes = useStyles();
	const [ userAnswerMap, setUserAnswerMap ] = useGlobalState('userAnswerMap');
	const [ urlObj, setUrlObj ] = useState();
	const [ showBack, setShowBack ] = useState(true);
	const [ showAnimation, setShowAnimation ] = useState(false);
	const [ userAnswer, setUserAnswer ] = useState({ first: '', second: '', op: '', answer: '' });
	const [ localUserAnswerMap, setLocalUserAnswerMap ] = useState(userAnswerMap);
	const [ hintLevel, setHintLevel ] = useState(0);

	const Question = ({ userAnswer }) => {
		const classes = useStyles();
		return (
			<div>
				<Typography
					id="wordQuestion"
					variant={'h4'}
					className={classes.question}
					dangerouslySetInnerHTML={{ __html: question._text }}
				/>
				<table className={classes.userAnswer}>
					<tbody>
						<tr>
							<td
								className={classes.userAnswerPart}
								style={{ background: hintLevel === 1 ? 'yellow' : 'none' }}
							>
								{userAnswer.first}
							</td>
							<td
								className={classes.userAnswerOp}
								style={{ background: hintLevel === 0 ? 'yellow' : 'none' }}
							>
								{userAnswer.op}
							</td>
							<td
								className={classes.userAnswerPart}
								style={{ background: hintLevel === 2 ? 'yellow' : 'none' }}
							>
								{userAnswer.second}
							</td>
							<td className={classes.userAnswerOp}>=</td>
							<td className={classes.userAnswerPart}>{userAnswer.answer}</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	};

	const showNext = () => {
		reset();
		setUrlObj(getRandomGif());
		setUserAnswer({ first: '', second: '', op: '', answer: '' });
		setShowAnimation(false);
		setShowBack(true);
		setHintLevel(0);
		setTimeout(() => setShowBack(false), 5 * 1000);
	};
	!answer && showNext();

	useEffect(
		() => {
			const ding = () => {
				playSound('ding');
				setHintLevel(hintLevel + 1);
			};
			const markCorrect = () => {
				playSound('fail');
				setShowAnimation(true);
				speak();
				setTimeout(() => {
					showNext();
					playSound('ding');
					setLocalUserAnswerMap([ ...localUserAnswerMap, getSmiley('happy') ]);
				}, 6000);
			};
			// the user entered the correct op, first or second
			if (hintLevel === 0) {
				if (userAnswer.op === op) ding();
				return;
			}
			if (hintLevel === 1) {
				if (userAnswer.first === first) ding();
				return;
			}
			if (hintLevel === 2) {
				if (userAnswer.second === second) ding();
				return;
			}
			// the user entered the correct answer
			if (hintLevel === 4 && userAnswer.answer === answer) markCorrect();
		},
		[ hintLevel, userAnswer, localUserAnswerMap ]
	);

	useEffect(() => () => setUserAnswerMap(localUserAnswerMap));

	const userAnswerChanged = (value) => {
		if (value.length > 3) return;
		if (hintLevel === 0)
			return [ '+', '-', 'x', '÷', '' ].includes(value)
				? setUserAnswer(Object.assign({}, userAnswer, { op: value }))
				: null;
		if (hintLevel === 1) return setUserAnswer(Object.assign({}, userAnswer, { first: value }));
		if (hintLevel === 2) return setUserAnswer(Object.assign({}, userAnswer, { second: value }));
		if (hintLevel === 3 && value === answer) {
			setUserAnswer(Object.assign({}, userAnswer, { answer: value }));
			setHintLevel(hintLevel + 1);
		}
	};

	if (hintLevel === 0) init = userAnswer.op;
	else if (hintLevel === 1) init = userAnswer.first;
	else if (hintLevel === 2) init = userAnswer.second;
	else init = userAnswer.answer;

	return (
		<div className={classes.root}>
			<TopNav heading="Word Problems" onShowNext={showNext} showBack={showBack} />
			{(hintLevel < 3 || showAnimation) && <Question userAnswer={userAnswer} />}
			{!showAnimation && (
				<div className={classes.keypad}>
					<Keypad init={init} invert={false} signs={hintLevel === 0} onChange={userAnswerChanged} />
				</div>
			)}
			{showAnimation && <Animator urlObj={urlObj} />}
			{<div className={classes.answer}>{answer}</div>}
			<BottomBar userAnswerMap={localUserAnswerMap} />
			{hintLevel >= 3 &&
			!showAnimation && (
				<div style={{ height: '100%' }}>
					{[ '+', '-' ].includes(op) && (
						<Add init={{ first, second, op, answer }} onCorrect={userAnswerChanged} />
					)}
					{[ 'x', '÷' ].includes(op) && (
						<Multiply init={{ first, second, op, answer }} onCorrect={userAnswerChanged} />
					)}
				</div>
			)}
		</div>
	);
};

export default Word;
