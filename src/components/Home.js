import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';

import { useGlobalState } from '../store';
import { speak, playSound, getRandomInt } from '../util';

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		height: '100%',
		borderCollapse: 'collapse'
	},
	block: {
		width: '50%',
		height: '50%',
		textAlign: 'center',
		border: 'none'
	},
	red: {
		background: '#e91e63'
	},
	green: {
		background: '#4caf50'
	},
	blue: {
		background: '#2196f3'
	},
	orange: {
		background: '#ff5722'
	},
	title: {
		color: 'white',
		fontSize: '2rem'
	}
}));

function Home() {
	const classes = useStyles();
	const [ , setScreen ] = useGlobalState('screen');
	const [ , setScreenColor ] = useGlobalState('screenColor');
	const [ , setTimesTableNumber ] = useGlobalState('timesTableNumber');
	const showNextScreen = (speech, screen, screenColor) => {
		playSound('click');
		speak(speech, () => {
			setScreen(screen);
			setScreenColor(screenColor);
		});
	};

	return (
		<table className={classes.root}>
			<tbody>
				{
					<tr>
						<td
							className={`${classes.block} ${classes.red}`}
							onClick={() =>
								showNextScreen('Okay. Lets do some Addition and Subtraction', 'ADD', '#e91e63')}
						>
							<Typography className={classes.title}>
								Addition<br />Subtraction
							</Typography>
						</td>
						<td
							className={`${classes.block} ${classes.green}`}
							onClick={() =>
								showNextScreen('Okay. Lets do some Division and Multiplication', 'MULTIPLY', '#4caf50')}
						>
							<Typography className={classes.title}>
								Division<br />Multiplication
							</Typography>
						</td>
					</tr>
				}
				<tr>
					<td
						className={`${classes.block} ${classes.blue}`}
						onClick={() => {
							showNextScreen('Okay. Lets do some Multiplication Tables', 'TABLES', '#2196f3');
							setTimesTableNumber(getRandomInt(6, 9));
						}}
					>
						<Typography className={classes.title}>
							Multiplication<br />Tables
						</Typography>
					</td>
					{
						<td
							className={`${classes.block} ${classes.orange}`}
							onClick={() => {
								showNextScreen('Okay. Lets do some Word problems', 'WORD', '#ff5722');
							}}
						>
							<Typography className={classes.title}>
								Word<br />Problems
							</Typography>
						</td>
					}
				</tr>
			</tbody>
		</table>
	);
}

export default Home;
